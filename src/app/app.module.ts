import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {UsersPage} from "../pages/users/users";
import {HttpModule} from "@angular/http";
import {UserPage} from "../pages/user/user";
import {BlacksPage} from "../pages/black-list/black-list";
import {BlackPage} from "../pages/black/black";
import {CarPage} from "../pages/car/car";
import {CarsPage} from "../pages/cars/cars";
import {RentsPage} from "../pages/rents/rents";
import {RentPage} from "../pages/rent/rent";
import { AuthService } from '../providers/auth-service/auth-service';
import {RegisterPage} from "../pages/register/register";
import {ProfilePage} from "../pages/profile/profile";

@NgModule({
  declarations: [
    MyApp,
    ProfilePage,
    HomePage,
    TabsPage,
    LoginPage,
    RegisterPage,
    UsersPage,
    UserPage,
    BlacksPage,
    BlackPage,
    RentsPage,
    RentPage,
    CarsPage,
    CarPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProfilePage,
    HomePage,
    TabsPage,
    LoginPage,
    RegisterPage,
    UsersPage,
    UserPage,
    BlacksPage,
    BlackPage,
    RentsPage,
    RentPage,
    CarsPage,
    CarPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService
  ]
})
export class AppModule {}
