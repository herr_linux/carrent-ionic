import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Http} from "@angular/http";
import 'rxjs/Rx';
import {BlackPage} from "../black/black";

@Component({
  selector: 'page-blacks',
  templateUrl: 'black-list.html'
})
export class BlacksPage {
  public http: Http
  public blacks
  public url = 'http://212.237.24.247:7000'
  constructor(public navCtrl: NavController, http: Http) {
    this.http = http
    this.getBlacks().subscribe(
        (data) => {
          this.blacks = data.data
        },
        (err) =>  console.log("Error Loging In:",err),
        () => { console.log("All Good With The Data")  }
      )
  }

  getBlacks(){
    return this.http.get(this.url + "/api/black").map(res => res.json())
  }

  goToBlack(id){
    return this.navCtrl.push(BlackPage, {
      id: id
    })
  }

}
