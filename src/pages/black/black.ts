import { Component } from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import {Http} from "@angular/http";
import 'rxjs/Rx';

@Component({
  selector: 'page-black',
  templateUrl: 'black.html'
})
export class BlackPage {
  public http: Http
  public black = {
    id: "",
    motif: "",
    level: "",
    fixed: 0,
    user: {
      image: "",
      firstname: "",
      lastname: "",
      email: "",
      tel: "",
      permis: ""
    }
  }
  public img = "assets/images/1.png"
  public url = 'http://212.237.24.247:7000'
  constructor(public navCtrl: NavController, http: Http, private navParam: NavParams, public loadingCtrl: LoadingController) {
    this.img = "assets/images/" + Math.floor((Math.random() * 4) + 1) + ".png"
    this.http = http
    this.getBlack().subscribe(
      (data) => {
        this.black = data.data

      },
      (err) =>  console.log("Error Loging In:",err),
      () => { console.log("All Good With The Data")  }
    )
    console.log(this.black)

  }

  getBlack(){
    return this.http.get(this.url + "/api/black/" + this.navParam.get('id')).map(res => res.json())
  }

}
