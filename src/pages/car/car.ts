import { Component } from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import {Http} from "@angular/http";
import 'rxjs/Rx';
import {RentPage} from "../rent/rent";

@Component({
  selector: 'page-car',
  templateUrl: 'car.html'
})
export class CarPage {
  public http: Http
  public car = {
    id: "",
    matricule: "",
    model: "",
    marque: "",
    moteur: "",
    prix: "",
    dateEntrer: "",
    dateVente: "",
    etat: "",
    image: "",
    locations: [],
    detail: {
    }
  }
  img = "assets/images/cars/car1.jpg"
  public url = 'http://212.237.24.247:7000'
  constructor(public navCtrl: NavController, http: Http, private navParam: NavParams, public loadingCtrl: LoadingController) {
    this.img =this.img = "assets/images/cars/car" + Math.floor((Math.random() * 3) + 1) + ".jpg"
    this.http = http
    this.getCar().subscribe(
      (data) => {
        this.car = data.data

      },
      (err) =>  console.log("Error Loging In:",err),
      () => { console.log("All Good With The Data")  }
    )
    console.log(this.car)

  }

  goToLocation(id){
    return this.navCtrl.push(RentPage, {
      id: id
    })
  }

  getCar(){
    return this.http.get(this.url + "/api/car/" + this.navParam.get('id')).map(res => res.json())
  }

}
