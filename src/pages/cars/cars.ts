import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Http} from "@angular/http";
import 'rxjs/Rx';
import {CarPage} from "../car/car";

@Component({
  selector: 'page-cars',
  templateUrl: 'cars.html'
})

export class CarsPage {
  public http: Http
  public cars = [{
    locations: []
  }]
  public url = 'http://212.237.24.247:7000'
  constructor(public navCtrl: NavController, http: Http) {
    this.http = http
    this.getCars().subscribe(
        (data) => {
          this.cars = data.data
          console.log("ok", this.cars)
        },
        (err) =>  console.log("Error Loging In:",err),
        () => { console.log("All Good With The Data")  }
      )
  }

  getCars(){
    return this.http.get(this.url + "/api/car").map(res => res.json())
  }

  goToCar(id){
    return this.navCtrl.push(CarPage, {
      id: id
    })
  }

}
