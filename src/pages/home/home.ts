import { Component } from '@angular/core';
import {NavController} from 'ionic-angular';
import {UsersPage} from "../users/users";
import {CarsPage} from "../cars/cars";
import {RentsPage} from "../rents/rents";
import {BlacksPage} from "../black-list/black-list";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public NavCtrl: NavController) {

  }

  goToPage(page){
    if(page == 'UsersPage')
      this.NavCtrl.push(UsersPage);
    else if(page == 'CarsPage')
      this.NavCtrl.push(CarsPage);
    else if(page == 'RentsPage')
      this.NavCtrl.push(RentsPage);
    else if(page == 'BlacksPage')
      this.NavCtrl.push(BlacksPage);
    else
      this.NavCtrl.push(HomePage);
  }
}
