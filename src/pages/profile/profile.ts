import { Component } from '@angular/core';
import {App, LoadingController, NavController} from 'ionic-angular';
import {Http} from "@angular/http";
import 'rxjs/Rx';
import {CarPage} from "../car/car";
import {AuthService} from "../../providers/auth-service/auth-service";
import {LoginPage} from "../login/login";

/**
 * Generated class for the ProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  public http: Http;
  public profile = {
    image: "",
    firstname: "",
    lastname: "",
    email: "",
    tel: "",
    permis: "",
    location: [{
      car: {
        marque: ""
      }
    }],
    black: null
  };
  user = {
    id: 0
  };
  public img = "assets/images/1.png";
  public url = 'http://212.237.24.247:7000';
  public app: App
  constructor(public navCtrl: NavController, http: Http, public loadingCtrl: LoadingController, private auth: AuthService, app: App) {
    this.img = "assets/images/" + Math.floor((Math.random() * 4) + 1) + ".png";
    this.app = app;
    this.user = this.auth.getUserInfo();
    this.http = http;
    this.getProfile().subscribe(
      (data) => {
        this.profile = data.data;
        console.log(this.profile)
      },
      (err) =>  console.log("Error Loging In:",err),
      () => { console.log("All Good With The Data")  }
    );
    console.log(this.profile)
  }

  goToCar(id){
    return this.navCtrl.push(CarPage, {
      id: id
    })
  }

  getProfile(){
    return this.http.get(this.url + "/api/user/" + this.user.id).map(res => res.json())
  }

  public logout() {
    this.auth.logout().subscribe(succ => {
      this.app.getRootNav().setRoot(LoginPage)
    });
  }

}
