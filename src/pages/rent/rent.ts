import { Component } from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import {Http} from "@angular/http";
import 'rxjs/Rx';
import {CarPage} from "../car/car";
import {UserPage} from "../user/user";

@Component({
  selector: 'page-rent',
  templateUrl: 'rent.html'
})
export class RentPage {
  public http: Http
  public rent = {
    image: "",
    firstname: "",
    lastname: "",
    email: "",
    tel: "",
    permis: "",
    car: {},
    client: {}
  }
  public url = 'http://212.237.24.247:7000'
  constructor(public navCtrl: NavController, http: Http, private navParam: NavParams, public loadingCtrl: LoadingController) {

    this.http = http
    this.getRent().subscribe(
      (data) => {
        this.rent = data.data

      },
      (err) =>  console.log("Error Loging In:",err),
      () => { console.log("All Good With The Data")  }
    )
    console.log(this.rent)

  }

  goToCar(id){
    return this.navCtrl.push(CarPage, {
      id: id
    })
  }

  goToUser(id){
    return this.navCtrl.push(UserPage, {
      id: id
    })
  }

  getRent(){
    return this.http.get(this.url + "/api/location/" + this.navParam.get('id')).map(res => res.json())
  }

}
