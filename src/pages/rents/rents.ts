import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Http} from "@angular/http";
import 'rxjs/Rx';
import {RentPage} from "../rent/rent";

@Component({
  selector: 'page-rents',
  templateUrl: 'rents.html'
})
export class RentsPage {
  public http: Http
  public rents
  public url = 'http://212.237.24.247:7000'
  constructor(public navCtrl: NavController, http: Http) {
    this.http = http
    this.getRents().subscribe(
        (data) => {
          this.rents = data.data
          console.log("ok", this.rents)
        },
        (err) =>  console.log("Error Loging In:",err),
        () => { console.log("All Good With The Data")  }
      )
  }

  getRents(){
    return this.http.get(this.url + "/api/location").map(res => res.json())
  }

  goToRent(id){
    return this.navCtrl.push(RentPage, {
      id: id
    })
  }
}
