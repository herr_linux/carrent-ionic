import { Component } from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import {Http} from "@angular/http";
import 'rxjs/Rx';
import {CarPage} from "../car/car";

@Component({
  selector: 'page-user',
  templateUrl: 'user.html'
})
export class UserPage {
  public http: Http
  public user = {
    image: "",
    firstname: "",
    lastname: "",
    email: "",
    tel: "",
    permis: "",
    location: [{
      car: {
        marque: ""
      }
    }],
    black: null
  }
  public img = "assets/images/1.png"
  public url = 'http://212.237.24.247:7000'
  constructor(public navCtrl: NavController, http: Http, private navParam: NavParams, public loadingCtrl: LoadingController) {
    this.img = "assets/images/" + Math.floor((Math.random() * 4) + 1) + ".png"
    this.http = http
    this.getUser().subscribe(
      (data) => {
        this.user = data.data
        console.log(this.user)
      },
      (err) =>  console.log("Error Loging In:",err),
      () => { console.log("All Good With The Data")  }
    )
    console.log(this.user)

  }

  goToCar(id){
    return this.navCtrl.push(CarPage, {
      id: id
    })
  }

  getUser(){
    return this.http.get(this.url + "/api/user/" + this.navParam.get('id')).map(res => res.json())
  }

}
