import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Http} from "@angular/http";
import 'rxjs/Rx';
import {UserPage} from "../user/user";

@Component({
  selector: 'page-users',
  templateUrl: 'users.html'
})
export class UsersPage {
  public http: Http
  public users
  public url = 'http://212.237.24.247:7000'
  constructor(public navCtrl: NavController, http: Http) {
    this.http = http
    this.getUsers().subscribe(
        (data) => {
          this.users = data.data
          console.log("ok", this.users)
        },
        (err) =>  console.log("Error Loging In:",err),
        () => { console.log("All Good With The Data")  }
      )
  }

  getUsers(){
    return this.http.get(this.url + "/api/user").map(res => res.json())
  }

  goToUser(id){
    return this.navCtrl.push(UserPage, {
      id: id
    })
  }

}
