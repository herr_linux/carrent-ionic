import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Http} from "@angular/http";

export class User {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  image: string;

  constructor(id: number, firstname: string, lastname: string, email: string, image: string) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.id = id;
    this.image = image;
  }
}

@Injectable()
export class AuthService {
  currentUser: User;
  public http: Http;
  public user: {
    id: 0,
    firstname: "",
    lastname: "",
    email: "",
    image: ""
  };
  public url = 'http://212.237.24.247:7000';
  constructor(http: Http){
    this.http = http

  }

  public login(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Inserer credentials, SVP");
    } else {

      return Observable.create(observer => {
        // At this point make a request to your backend to make a real check!
        this.getUser(credentials.email, credentials.password).subscribe(
          (data) => {
            this.user = data.data

            let access = (this.user);
            if(this.user){
              this.currentUser = new User(this.user.id, this.user.firstname, this.user.lastname, this.user.email, this.user.image);
            }
            else{
              this.currentUser = null;
            }
            observer.next(access);
            observer.complete();
          },
          (err) =>  console.log("Error Loging In:",err),
          () => { console.log("All Good With The Data")  }
        )
      });
    }
  }

  public getUser(email, pass){
    return this.http.get(this.url + "/api/user/login?email=" + email + "&password=" + pass).map(res => res.json())
  }

  public register(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      // At this point store the credentials to your backend!
      return Observable.create(observer => {
        observer.next(true);
        observer.complete();
      });
    }
  }

  public getUserInfo() : User {
    return this.currentUser;
  }

  public logout() {
    return Observable.create(observer => {
      this.currentUser = null;
      observer.next(true);
      observer.complete();
    });
  }
}
